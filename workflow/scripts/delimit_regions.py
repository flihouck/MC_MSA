import sys

""" 
A script used to extract the largest regions where the depth is higher than a specified depth
 inputs : the input (samtools depth output format) path, the output file path and the target depth
 output : a file with regions written in this format [ 'start1_end1', 'start2_end2' ... ]
"""


def main(args):
    start_r = 0
    regions = []
    amount = int(args[3])
    with open(args[1], 'r') as f:
        for line in f:
            line.replace("\n", '')
            pos, depth = line.split("\t")
            if int(depth) > amount:
                if start_r == 0:
                    start_r = pos
            else:
                if start_r != 0:
                    regions.append((str(start_r), str(int(pos) - 1)))
                    start_r = 0
    with open(args[2], 'w') as out:
        out.write('[')
        for elem in regions:
            out.write('"' + elem[0] + "_" + elem[1] + '",')
        out.write(']')


if __name__ == "__main__":
    if len(sys.argv) == 4:
        main(sys.argv)
    else:
        sys.exit("Usage:\ndelimit_regions.py input output min_depth\n")
