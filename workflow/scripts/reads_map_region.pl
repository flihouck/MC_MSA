#!/usr/bin/perl -w
use List::Util qw(min max);

#------------------------------------------------------------------------------
# VERIFICATION D'USAGE
#------------------------------------------------------------------------------
sub usage{
	print "\nRead_map_region uses a SAM file to retrieve in FASTA format the reads\n".
		  "mapped to a specific region of the reference. Reads are truncated to\n".
		  "retrieve only the mapped section but this step can be removed with the\n".
			"uncut-end option.\n".
		  "\nUSAGE: ./read_map_region.pl [option] -s <int> -t <int> <in.sam> <output>\n".
	      "\t-s starting position of the region\n".
	      "\t-t size of the region\n".
	      "\t-e end position of the region (This option replaces the -t)\n".
	      "\nOPTION(S):\n".
	      "\t-c percentage of coverage from which the read is kept (default: 100)\n".
	      "\t-u --uncut-end (always put first) do not cut the ends of the read\n".
				"\t                not mapped to the region\n".
	      "\n";
	exit;
}

if (!exists $ARGV[0] || $ARGV[0] =~ '-h'){
	usage();
}

if ( $ARGV[0] =~ /(-u|--uncut-end)/ ){
	$UNCUT_END = 1;
	shift(@ARGV);
}
else{
	$UNCUT_END = 0;
}

$OUTPUT = pop(@ARGV);
$FILE_INPUT = pop(@ARGV);
@TAB_FILE_INPUT = split(/\./,$FILE_INPUT);
$FORMAT = pop(@TAB_FILE_INPUT);
if ( !open(IN, "<", $FILE_INPUT) || $FORMAT ne 'sam' ){
	print "The file could not be opened or the format is incorrect.\n";
	usage();
}

%hash_argv = @ARGV;

if ( (exists $hash_argv{'-s'}) && ($hash_argv{'-s'} =~ /[0-9]+/) ) {
    $REF_START = $hash_argv{'-s'}*1;
}
else{
    print "The beginning of the region must be indicated.\n";
    usage();
}

if ( (exists $hash_argv{'-t'}) && ($hash_argv{'-t'} =~ /[0-9]+/) ){
    $REGION_SIZE = $hash_argv{'-t'}*1;
    $REF_END = $REF_START + $REGION_SIZE - 1;
}
else{
	if ( (exists $hash_argv{'-e'}) && ($hash_argv{'-e'} =~ /[0-9]+/) ){
		$REF_END = $hash_argv{'-e'}*1;
		$REGION_SIZE = $REF_END - $REF_START + 1;
	}
	else{
		print "The size of the region or the end of the region must be indicated.\n";
		usage();
	}
}

if (exists $hash_argv{'-c'}){
	$COVERAGE = $hash_argv{'-c'};
	if ($COVERAGE > 100 || $COVERAGE < 0){
		print "The coverage must be between 0 and 100.\n";
		usage();
	}
}
else{
	$COVERAGE = 100;
}

#------------------------------------------------------------------------------
# DEBUT DU SCRIPT
#------------------------------------------------------------------------------

#Fonction: Renvoie true si le byte est présent dans l'octet
#Arg: $FLAG int flag/octet à tester
#			$power int puissance du byte rechercher
#Renvoie: boolean
sub checksflag
{
	my ($FLAG,$power)=@_;
	return ((($FLAG/(2**$power)) % 2) != 0);
}

open(OUT, ">", $OUTPUT) || die ("You cannot create the file $OUTPUT");
#Lecture du fichier sam
while (<IN>)
{
  unless(/^@/)
  {
  	chomp;
    ($QNAME,$FLAG,$RNAME,$pos_start,$MAPQ,$CIGAR,$RNEXT,$PNEXT,$TLEN,$SEQ) = split(/\t/,$_);

		#Ceux qui match et ont un alignement primaire
		if (!checksflag($FLAG,2) && !checksflag($FLAG,8)){
			#print("$QNAME $FLAG\n");
	    #Ceux qui match avant la fin de la région
	    if ($pos_start <= $REF_END) {
	      @nb_cigar = split(/[A-Z]/,$CIGAR);
	      @op_cigar = split(/[0-9]*/,$CIGAR);
	      shift(@op_cigar);
	      $nb_op_cigar = @op_cigar;
	      $pos_end = $pos_start-1;#Le premier nucléotide est utilisé
	      $i = 0;
	      $nb_insertion = 0;
	      $nb_deletion = 0;
				$nb_segment = 0;
	      $read_finish = 0;
	      #Read qui commence avant la région
	      if ($pos_start < $REF_START){
					#Tant que le cigar n'est pas fini ou que le début de la région n'est pas dépassé
					while ($i < $nb_op_cigar && $pos_end < $REF_START-1) {
						if (!($op_cigar[$i] =~ "H" )) {
							if ($op_cigar[$i] =~ 'I'){
								$nb_insertion += $nb_cigar[$i];
							}
							else{
								if ($op_cigar[$i] =~ 'S'){
									$nb_segment += $nb_cigar[$i];
								}
								else{
									$pos_end += $nb_cigar[$i];
									if ($op_cigar[$i] =~ 'D'){
									$nb_deletion += $nb_cigar[$i];
									}
								}
							}
						}
						$i++;
					}

					$diff_pressure_cigar = $pos_end - ($REF_START -1) ;

					#Si la coupure ne ce fait pas entre deux opérateurs du cigar
					if ($diff_pressure_cigar > 0){
						$i--;
						$pos_end -= $diff_pressure_cigar;
						$nb_cigar[$i] = $diff_pressure_cigar;
						$nb_deletion -= $diff_pressure_cigar if ($op_cigar[$i] =~ 'D');
					}else{
						#Si le read n'est pas fini
						if ($i < $nb_op_cigar){
							#S'il y a une insertion sur le prochain opérateur
							#pour pouvoir couper après
							if ($op_cigar[$i] =~ 'I'){
								$nb_insertion += $nb_cigar[$i];
								$i++;
							}
						}
						#Sinon, le read match avant la région
						else{
							$read_finish = 1;
						}
					}


					$start_match_on_read = $pos_end - $pos_start + $nb_insertion + $nb_segment - $nb_deletion +1;

	      }else{
	        $start_match_on_read=0;
	      }

	      #Elimine ceux qui termine avant la région
	      #après cette condition tout les reads sont bien mappé sur la région
	      if (!$read_finish) {
					#Continue le parcours du cigar, s'arrête si la fn de la région est rencontré
	        while ($i < $nb_op_cigar && $pos_end < $REF_END) {
						if (!($op_cigar[$i] =~ 'H')) {
		          if ($op_cigar[$i] =~ 'I'){
		            $nb_insertion += $nb_cigar[$i];
		          }
		          else{
								if ($op_cigar[$i] =~ 'S'){
									$nb_segment += $nb_cigar[$i];
								}
								else{
									$pos_end += $nb_cigar[$i];

									if ($op_cigar[$i] =~ 'D'){
									$nb_deletion += $nb_cigar[$i];
									}
								}
		          }
						}
	          $i++;
	        }
					#Si la coupure ne ce fait pas entre deux opérateurs du cigar
					$diff_pressure_cigar = $pos_end - $REF_END ;
					if ($diff_pressure_cigar > 0){
						$i--;
						$pos_end -= $diff_pressure_cigar;
						$nb_cigar[$i] = $diff_pressure_cigar;
						$nb_deletion -= $diff_pressure_cigar if ($op_cigar[$i] =~ 'D');
					}
					$end_match_on_read = $pos_end - $pos_start + $nb_insertion + $nb_segment - $nb_deletion;

	        $region_coverage = max($REF_END,$pos_end) - min($REF_START,$pos_start) - abs($REF_END - $pos_end) - abs($REF_START - $pos_start) + 1;
					if ($region_coverage >= $REGION_SIZE*($COVERAGE/100) ) {
	        	if ($UNCUT_END){
							print OUT ">$QNAME\n";
	        		print OUT "$SEQ\n";
	        	}
	        	else{
							$truncated_seq_size = $end_match_on_read - $start_match_on_read;
							if ($truncated_seq_size > $REGION_SIZE/2) {
								print OUT ">$QNAME\n";
								@seq = split(//,$SEQ);
								for (my $j = $start_match_on_read; $j <= $end_match_on_read; $j++) {
									print OUT "$seq[$j]";
								}
								print OUT "\n";
							}
			      }
	        }#else{print("$QNAME $FLAG Pas assez couvert\n")}
	      }#else{print("$QNAME $FLAG termine avant\n")}
	    }#else{print("$QNAME $FLAG match après\n")}
		}#else{print("$QNAME $FLAG ignoré\n")}
  }
}
close IN;

$MAPQ=$PNEXT=$RNAME=$TLEN=$RNEXT=0;
