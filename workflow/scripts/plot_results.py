#!/usr/bin/env python3


import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import re
import sys
import os


def parse_file(filename, x_scale):
    identity, similarity, equivalence, mismatches, length, aln_length = [ [], [] , [], [], [],[]]
    value = []

    with open(filename, 'r') as f:
        cnt= 0
        for line in f:
            try:
                i, s, e, m, tl, al = map(float, line.split(',')[1:])
            except:
                i,s,e,m, tl, al = [0, 0, 0, 0, 0, 0]
                for x in range(1, len(line.split(','))-1):
                    print("Missing values replaced by 0, 0, 0, 0, 0, 0")
                                    
            identity.append(i)
            similarity.append(s)
            equivalence.append(e)
            mismatches.append(m)
            length.append(tl)
            aln_length.append(al)
            cnt+=1
    return identity, similarity, equivalence, mismatches, length, aln_length 


def plot_from_folder(args, target, labels, folder):

    ls, li, le, lm, ltl, lal = [], [], [], [], [], []
    for tool_file in args:
        if folder != "":
            cond =  ".txt" in tool_file.name
            filename = folder+tool_file.name
        else:
            cond = ".txt" in tool_file
            filename= tool_file
        if cond :
            i, s, e, m, tl, al = map(np.array, parse_file(filename, th))
            li.append(i)
            ls.append(s)
            le.append(e)
            lm.append(m)
            ltl.append(tl)
            lal.append(al)
    fig = plt.figure(figsize=(20,10))
    ll = [ls, li, le, lm, ltl, lal]
    graph_cnt = 0
    target_att = ["identity", "similarity", "equivalence", "mismatches", "sequence_length", "aligned_length"]
    for graph in ll:
        plt.clf()
        tab_cnt=0
        for tab in graph:
            if (len(tab) == len(th)):
                plt.plot(th, tab, 'o--',  linewidth=1, label=labels[tab_cnt])
                
                #    plt.plot(th,tab, label=labels[tab_cnt])
            else:
                plt.plot(th,[tab for x in range(len(th))], label=labels[tab_cnt])
            tab_cnt+=1
        legend = plt.legend(shadow=True, fontsize='x-large')
        plt.savefig(target+"_" + target_att[graph_cnt] + ".png", format="png")
        graph_cnt+=1

if __name__ == "__main__":
    th = list(map(int, sys.argv[2][1:-1].split(" ")))

    if (len(sys.argv) < 4):
        exit(-1)
    if (len(sys.argv) < 4):
        print(sys.argv)
        
        folder_it = os.scandir(sys.argv[3])
        name= [ x.name for x in folder_it]
        #folder_it= os.scandir(sys.argv[2])
        plot_from_folder(folder_it, sys.argv[1], name, folder=sys.argv[3])
        exit(0)
    lb= []
    for elem in th:
         lb.append("th "+str(elem))
    plot_from_folder(sys.argv[3:] ,sys.argv[1], sys.argv[3:], folder="")


