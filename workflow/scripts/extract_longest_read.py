#!/usr/bin/env python3

import sys
import argparse

def extract_longest_read(filename, nb_reads):
    max_len = [0 for x in range(nb_reads)]
    descr= ""
    longest_read = ["" for x in range(nb_reads)]
    is_descr = True
    with open(filename, 'r') as f:
        for line in f:
            if not is_descr:
                min_len = min(max_len)
                if len(line) > min_len and line[0] in ['A','C','G','T','N']:
                    index = max_len.index(min_len)
                    max_len[index] = len(line)
                    longest_read[index] = descr + line
                is_descr = True
            elif (line[0] == '>' or line[0] == '@'):
                is_descr = False
                descr = ">" +line
                
    return longest_read

def main():
    parser=argparse.ArgumentParser()
    parser.add_argument("-input", help="the file to extract reads from", required=True)
    parser.add_argument("-number", help="the amount of reads to extract (default=1)")
    parser.add_argument("-output", help="the output file's name (the reads will be printed to stdout otherwise)") 
    amount = 1
    args = parser.parse_args()
    if args.number:
        try:
            amount = int(args.number)
        except:
            sys.exit("'-n' requires a number")
    results = extract_longest_read(args.input, amount)
    if args.output:
        output= args.output
        with open(output, 'w') as f:
    	    for read in results:   
                f.write("".join(read))
    else:
        for read in results:
            print("".join(read))
    
if (__name__ == "__main__"):
    main()
